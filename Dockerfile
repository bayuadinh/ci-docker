FROM alpine:latest

RUN apk add --update nginx curl && \
    apk add --no-cache \
    rm -rf /var/cache/apk/* && \
    mkdir -p /var/www/html/

COPY nginx.conf /etc/nginx/nginx.conf
COPY conf.d/. /etc/nginx/conf.d/.
COPY index.html /var/www/html/index.html

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]